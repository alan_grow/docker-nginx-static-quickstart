Serving Static Assets with NGINX on Docker
==========================================

By Alan W  
April 6, 2020  

This tutorial shows you how to run an NGINX server in a Docker
container. Web developers who are getting into (or required) to do
some [DevOps](https://en.wikipedia.org/wiki/DevOps) stuff may find
this document helpful.

In broad strokes, **Docker** allows you to use things similar to
little operating system in boxes ("**containers**") so all potential
trouble is contained in isolated (virtual) spaces. Docker itself makes
use of the host system's resources, which the containers it is running
may draw upon. In this way, Docker is more efficient than doing things
like running a separate Linux virtual machine for each deployed web
app.

**NGINX** is a web server that can run in a Docker container. For
local development, this means you can point your browser (client) to
somewhere like http://localhost:8000 and get a *response* from your
web server. We'll configure **NGINX** to spit back an `index.html`
file.

Installing Stuff
----------------

Please have Docker installed. I am writing this from Debian 10 (Linux)
running in VirtualBox on Windows 10 Home. If you are using Windows 10
Home, you might have to do some kind of virtualization like what I'm
doing in order to run Docker. Then you can keep your video games on
Windows and still get work done in Linux... ha!

What's here
-----------

Docker is configured in a `Dockerfile`. Take a look at the one
included in this repository.

Included in this configuration file are **instructions** telling
Docker which **image** (think: installation CD/DVD/`.iso` provided by
an 'official' producer for getting some base software) to build
`FROM`, `COPY` to copy-pasta things from your source repository, and
other (UNIX-y) commands to `RUN`. See the official [Dockerfile
reference](https://docs.docker.com/engine/reference/builder/) for more
details.

In this example, I just serve up the static assets (like HTML, CSS,
and JavaScript files) in the `/public/` folder contained in this
repository. Rather than going directly from this repository to your
web browser (you *could* just click on the `index.html` file to see it
in your browser), these files must be copy-pasta'd to a Docker
container running NGINX. NGINX is configured by default to serve up
static assets from the directory we copy our `/public/` folder to.

Building the Container
----------------------

From this (root) directory, run

```bash
docker build --tag foo-content-nginx .
```

You can change `foo-nginx` to something non-silly in the command
above. Then do:

Running the Container
---------------------

```bash
docker run --name foo-nginx -d -p 8080:80 foo-content-nginx
```

Go to http://localhost:8080 on your browser and you should see the
static assets in the index.html file.
You can run `docker stats` to see that your container is running too.

To delete your thing do, `docker rm --force foo-nginx`. After doing
this, if you try to navigate to http://localhost:8080, you will not
see the static content from the `/public/` directory.

Summary
-------

In this very short tutorial, we went over doing some basic tasks in
Docker. Easier than installing/configuring an Apache server or
something, no?

To get more help, you can look at Docker itself via `docker --help` to
see all the things it can do, searching relevant topics of interest
online.

